import React, {useContext, useEffect, useState} from 'react';
import logo from './logo.svg';
import './App.css';
import {Box, Button} from "@mui/material";
import {useRSocket} from "./useRScocket";
import {GeneralContext} from "./context/GeneralContext";

function App() {
    const { likes, setLikes } = useContext<any>(GeneralContext);
    const { clickPublish, actionPublish, requestStream} = useRSocket();
    const [likeCount, setLikeCount] = useState(0);

    const handlerPublish = () => {
        setLikeCount(() => likeCount + 1);
        // connectionLike();
        clickPublish({action: 'like'});
    }

    const handlerAction = (action: string) => {
        // connectionLike();
        actionPublish({action: action});
    }


    const handlerRequestStream = () => {
        // setLikeCount(() => likeCount + 1);
        // connectionLike();
        requestStream({message: 'hola'}, handleChange);
    }


    const handleChange = async (data: any) => {
        setLikes(data);
    }

    useEffect(() => {
        // requestResponse({message: 'hola'});
    }, [])

    return (
        <div className="App">
            <header className="App-header">
                <img src={logo} className="App-logo" alt="logo"/>
                <p>
                    Edit <code>src/App.tsx</code> and save to reload.
                </p>
                <a
                    className="App-link"
                    href="https://reactjs.org"
                    target="_blank"
                    rel="noopener noreferrer"
                >
                    Learn React
                </a>
            </header>
            <Box sx={{position: 'absolute', top: 20, left: 20, width: '100%', display: 'flex', justifyContent: 'space-around'}}>
                {/*<Button onClick={() => handlerPublish()} variant="contained">clic {likes}</Button>*/}
                <Button onClick={() => handlerAction('LIKE')} variant="contained">Like {likes}</Button>
                <Button onClick={() => handlerPublish()} variant="contained">Clic {likes}</Button>
                <Button onClick={() => handlerAction('SHARE')} variant="contained">Share {likes}</Button>
                {/*<Button onClick={() => handlerAction('CONN')} variant="contained">Conn {likes}</Button>*/}
                <Button onClick={() => handlerAction('REGULAR_CLICK')} variant="contained">Regular clic {likes}</Button>
                <Button onClick={() => handlerAction('POPUP_CLICK')} variant="contained">Popup clic {likes}</Button>
                <Button onClick={() => handlerRequestStream()} variant="contained">RequestStream </Button>
                {/*<Button onClick={() => handlerRequestResponse()} variant="contained">RequestResponse {likes}</Button>*/}
            </Box>
        </div>
    );
}

export default App;
