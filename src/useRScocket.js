import {
    IdentitySerializer,
    JsonSerializer,
    RSocketClient
} from "rsocket-core";
import RSocketWebSocketClient from "rsocket-websocket-client";
import {useEffect} from 'react';
import moment from "moment";
// 'https://dev-firaconnectionms.xdn-fira-live.com'
const url1 = 'wss://dev-firaconnectionms.xdn-fira-live.com'
const url2 = 'ws://localhost:8080'

const transport = new RSocketWebSocketClient({
    url: `${url2}/connection/event`,
    debug: true,
    wsCreator: (url) => {
        return new WebSocket(url);
    }
})

// @ts-ignore
let socket = {};
// const broadcastingId = '64e42eb5e8eb9255691ac00d';
const broadcastingId = '64e60cd2e8eb9255691ac01d';
let connectionId = '';

const client = new RSocketClient({
    transport,
    serializers: {
        data: JsonSerializer,
        metadata: IdentitySerializer,
    },
    setup: {
        keepAlive: 10000000,
        lifetime: 10000000,
        dataMimeType: 'application/json',
        metadataMimeType: 'message/x.rsocket.routing.v0',
    },
});

const connect = () => {
    client.connect().subscribe({
        onComplete: s => {
            socket = s;
            requestResponse();
        },
        onError: error => {
            console.error(error);
        },
    });
};

const requestResponse = ( ) => {
    socket.requestResponse({
        data: {
            broadcastingId,
            connectionType: "WEB"
        },
        metadata: String.fromCharCode("new".length) + "new",
    }).subscribe({
        onComplete: payload => {
            console.log(payload.data);
            connectionId = payload.data.connectionId;
        },
        onError: error => {
            console.error(error);
        },
    });
};

const clickPublish = (data ) => {
    socket.fireAndForget({
        data: {
            connectionId,
            broadcastingId: broadcastingId,
            actionType: 'LIKE',
            actionTime: new Date().toUTCString()
        },
        metadata: String.fromCharCode(`click.publish`.length) + `click.publish`,
    });
};

const actionPublish = (data ) => {
    socket.fireAndForget({
        data: {
            connectionId,
            broadcastingId: broadcastingId,
            actionType: data.action,
            actionTime: moment(new Date().toUTCString())
        },
        metadata: String.fromCharCode(`action.publish`.length) + `action.publish`,
    });
};




const requestStream = ( data, handleChange ) => {
    console.log('requestStream');
    console.log(connectionId);
    const actions = ['lk', 'sh', 'conn', 'rpc', 'ppc'];
    // callRequestStream('lk')
    for (const action of actions) {
        callRequestStream(action);
    }
}

const callRequestStream = (action) => {
    socket.requestStream({
        metadata: String.fromCharCode(`${action}.${broadcastingId}.${connectionId}`.length) + `${action}.${broadcastingId}.${connectionId}`,
    }).subscribe({
        onNext: payload => {
            // Aquí puedes procesar cada dato que recibes del servidor en tiempo real
            console.log(payload.data);
            // handleChange(payload.data)
        },
        onComplete: () => {
            // Aquí puedes hacer algo cuando el canal se cierra
            console.log('Canal cerrado');
        },
        onError: error => {
            // Aquí puedes manejar los errores que ocurran
            console.error(error);
        },
        onSubscribe: (sub) => sub.request(2147483647)
    });
}

export const useRSocket = () => {

    useEffect(() => {
        connect();
        return () => {
            socket.close();
        };
    }, []);

    return { requestResponse, clickPublish, actionPublish, requestStream };
};