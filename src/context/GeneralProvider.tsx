import { GeneralContext } from "./GeneralContext"
import {useState} from "react";


export const GeneralProvider = ({ children }: any) => {
    const [likes, setLikes] = useState(0);

    const data = {
        likes, setLikes
    }

    return (
        <GeneralContext.Provider value={ data } >
            { children }
        </GeneralContext.Provider>
    )
}