import {
    encodeCompositeMetadata, encodeRoute,
    IdentitySerializer,
    JsonSerializer,
    MESSAGE_RSOCKET_COMPOSITE_METADATA,
    RSocketClient
} from "rsocket-core";
import RSocketWebSocketClient from "rsocket-websocket-client";
import {ReactiveSocket} from "rsocket-types";
import {Buffer} from "buffer";

// url: "wss://dev-firaconnectionms.xdn-fira-live.com/connection/event",
const transport = new RSocketWebSocketClient({
    url: "ws://localhost:8080/connection/event",
})
// https://dev-firaconnectionms.xdn-fira-live.com/
const client = new RSocketClient({
    transport,
    serializers: {
        data: JsonSerializer,
        metadata: IdentitySerializer,
    },
    setup: {
        keepAlive: 180000,
        lifetime: 180000,
        dataMimeType: 'application/json',
        metadataMimeType: 'message/x.rsocket.routing.v0',
    },
});
const broadcastingId = '1565465165464';


export const connectionsDemo = () => {
    client.connect().subscribe({
        onComplete: socket => {
            socket.requestResponse({
                data: {
                    broadcastingId,
                    connectionType: "WEB"
                },
                metadata: String.fromCharCode("new".length) + "new",
            }).subscribe({
                onComplete: payload => {
                    console.log("Conexión Event realizada con éxito");
                    console.log(payload.data);
                },
                onError: error => {
                    console.error(error);
                },
                onSubscribe: cancel => {
                    /* call cancel() to abort */
                }
            })
            socket.fireAndForget({
                data: broadcastingId,
                metadata: String.fromCharCode(`lk.${broadcastingId}`.length) + `lk.${broadcastingId}`
            })
        }
    })
}

export const connection = async () => {
    console.log('connect');
    const socket = await client.connect();
    const requestNew = {
        data: {
            broadcastingId,
            connectionType: "WEB"
        },
        metadata: String.fromCharCode("new".length) + "new",
    };

    socket.requestResponse(requestNew).subscribe({
        onComplete: response => {
            console.log("Connection data {}", response);
            console.log("Conexión Event realizada con éxito");
            connections(socket)
        },
        onError: error => {
            console.error("Error enviando solicitud", error.message);
        },
    });

}

const connections = (socket: ReactiveSocket<any, any>) => {
    connectionLike(socket);
    // connectionShare(socket);
    // connectionNewProduct(socket);
    // connectionConn(socket);
}

export const connectionLike = async (socket: ReactiveSocket<any, any>) => {
    console.log('like');

    const requestLike = {
        // data: {
        //     broadcastingId
        // },
        metadata: String.fromCharCode(`lk.${broadcastingId}`.length) + `lk.${broadcastingId}`,
    };

    // socket.requestResponse(requestLike).subscribe({
    //     onComplete: response => {
    //         console.log("Connection data {}", response);
    //         console.log("Conexión Like realizada con éxito");
    //     },
    //     onError: error => {
    //         console.error("Error enviando solicitud", error.message);
    //     },
    // });

    socket.fireAndForget({
        data: Buffer.from(broadcastingId),
        metadata: encodeCompositeMetadata([
            [MESSAGE_RSOCKET_COMPOSITE_METADATA, encodeRoute(`lk.${broadcastingId}`)]
        ])
    })

    // socket.requestStream(requestLike).subscribe({
    //     onComplete: () => {
    //         console.log("Conexión like realizada con éxito");
    //     },
    //     onNext: payload => {
    //         console.log("Respuesta like :", payload);
    //         if (!payload.data.code) {
    //             console.log("conteo like :", payload.data);
    //         }
    //         // handleLike(values.data);
    //     },
    //     onError: error => {
    //         console.error("Error enviando solicitud de likes", error);
    //     },
    //     onSubscribe: subs => {
    //         subs.request(10);
    //         // console.log(subs);
    //         // if(++c === 10)
    //     }
    // });
}

const connectionShare = async (socket: ReactiveSocket<any, any>) => {
    console.log('share');
    let c = 0;
    const requestShare = {
        data: {
            broadcastingId
        },
        metadata: String.fromCharCode(`sh.${broadcastingId}`.length) + `sh.${broadcastingId}`,
    };

    socket.requestStream(requestShare).subscribe({
        onComplete: () => {
            console.log("Conexión Share realizada con éxito");
        },
        onNext: payload => {
            console.log("conteo Share :", payload);
            // handleLike(values.data);
        },
        onError: error => {
            console.error("Error enviando solicitud de shares", error);
            // reject(error);
        },
        onSubscribe: subs => {
            subs.request(10);
            // console.log(subs);
            // if(++c === 10)
            // resolve(subs);
        }
    });
}

const connectionNewProduct = async (socket: ReactiveSocket<any, any>) => {
    console.log('new Product');
    let c = 0;
    const requestShare = {
        data: {
            broadcastingId
        },
        metadata: String.fromCharCode(`np.${broadcastingId}`.length) + `np.${broadcastingId}`,
    };

    socket.requestStream(requestShare).subscribe({
        onComplete: () => {
            console.log("Conexión new Product realizada con éxito");
        },
        onNext: payload => {
            console.log("conteo new Product :", payload);
            // handleLike(values.data);
        },
        onError: error => {
            console.error("Error enviando solicitud de new Products", error);
            // reject(error);
        },
        onSubscribe: subs => {
            subs.request(10);
            // console.log(subs);
            // if(++c === 10)
            // resolve(subs);
        }
    });
}

const connectionConn = async (socket: ReactiveSocket<any, any>) => {
    console.log('Conn');
    let c = 0;
    const requestShare = {
        data: {
            broadcastingId
        },
        metadata: String.fromCharCode(`conn.${broadcastingId}`.length) + `conn.${broadcastingId}`,
    };

    socket.requestStream(requestShare).subscribe({
        onComplete: () => {
            console.log("Conexión Conn realizada con éxito");
        },
        onNext: payload => {
            console.log("conteo Conn :", payload);
            // handleLike(values.data);
        },
        onError: error => {
            console.error("Error enviando solicitud de Conns", error);
            // reject(error);
        },
        onSubscribe: subs => {
            subs.request(10);
            // console.log(subs);
            // if(++c === 10)
            // resolve(subs);
        }
    });
}